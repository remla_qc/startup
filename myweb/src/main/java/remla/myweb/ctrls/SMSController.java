package remla.myweb.ctrls;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mylib.RemlaUtil;
import remla.myweb.data.Sms;
import remla.myweb.data.SmsClassification;
import java.util.*;
import java.io.*;

@Controller
@RequestMapping(path = "/sms")
public class SMSController {

	private final RestTemplateBuilder rest;
	private final String modelHost;
	private final HelloWorldCtrl hws;

	public SMSController(RestTemplateBuilder rest, Environment env, HelloWorldCtrl hws) {
		this.hws = hws;
		modelHost = env.getProperty("MODEL_HOST");
		System.out.printf("Using '%s' as model host.\n", modelHost);
		this.rest = rest;
	}

	@GetMapping("/accuracy")
	public String getModelAccuracy(){
		int numberOfRecords = 100;
		int correct = 0;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("spam.csv");
		InputStreamReader r = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(r);
		String line;
		int counter = 0;
		System.out.println("Calculating accuracy...");
		try{
			while((line = br.readLine()) != null && counter < numberOfRecords){
				try{
					String[] values = line.split(",");
					Sms sms = new Sms();
					sms.guess = values[0];					
					sms.sms = values[1];

					String url = modelHost + "/predict";
					ResponseEntity<SmsClassification> res = rest.build().postForEntity(url, sms, SmsClassification.class);

					String pred = res.getBody().result.toLowerCase().trim();
					if (pred.equals(sms.guess)) {
						correct++;
					}
					counter++;
				}  catch (ArrayIndexOutOfBoundsException ex){
					System.out.println(ex);
				}
			}
		} catch (IOException ex){
			System.out.println(ex);
		}
		double ret = (double) correct / numberOfRecords ;
		System.out.println(ret);
		this.hws.setModelAccuracy(ret);
		return "/";
	}

	@GetMapping("/")
	public String index(Model model) {
		hws.addAccess();
		model.addAttribute("hostname", RemlaUtil.getHostName());
		return "sms/index";
	}

	@PostMapping("/")
	@ResponseBody
	public SmsClassification predict(@RequestBody Sms q) {
		hws.addAccess();

		String url = modelHost + "/predict";
		ResponseEntity<SmsClassification> res = rest.build().postForEntity(url, q, SmsClassification.class);

		String pred = res.getBody().result.toLowerCase().trim();
		String guess = q.guess.toLowerCase().trim();
		hws.addPrediction(pred.equals(guess));

		return res.getBody();
	}
}
